import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';

Vue.use(Vuex);

const vuexLocalStorage = new VuexPersist({
  key: 'vuex',
  storage: window.localStorage,
});

export default new Vuex.Store({
  plugins: [vuexLocalStorage.plugin],
  state: {
    counter: 0,
    bosses: [],
  },
  mutations: {
    SET_COUNTER(state, payload) {
      state.counter = payload;
    },
    SET_BOSSES(state, payload) {
      state.bosses = payload;
    },
    SET_BOSS_COUNTER(state, { bossIndex, count }) {
      state.bosses[bossIndex].count = count;
    },
  },
  actions: {
    increment({ state, commit }) {
      commit('SET_COUNTER', state.counter + 1);
    },
    bossIncrement({ state, commit }, payload) {
      const bossIndex = state.bosses.findIndex(({ name }) => name === payload);
      commit('SET_BOSS_COUNTER', {
        bossIndex,
        count: state.bosses[bossIndex].count + 1,
      });
    },
    decrement({ state, commit }) {
      commit('SET_COUNTER', state.counter - 1);
    },
    bossDecrement({ state, commit }, payload) {
      const bossIndex = state.bosses.findIndex(({ name }) => name === payload);
      commit('SET_BOSS_COUNTER', {
        bossIndex,
        count: state.bosses[bossIndex].count - 1,
      });
    },
    addBoss({ state, commit }, payload) {
      commit('SET_BOSSES', state.bosses.concat(payload));
    },
  },
  getters: {
    getFullCount: (state) => state.bosses
      .map((boss) => boss.count)
      .reduce((a, b) => a + b, 0) + state.counter,
  },
});
